;; f(x) = 1 / ( ⌊x⌋ + 1 − {x} )
;; This function represents all rational numbers
;; see https://twitter.com/TamasGorbe/status/1571609944192913410
(defparameter *rationals*
  (let ((a 0))
    (flet ((f (x)
             (multiple-value-bind (integer-part fractional-part)
                 (floor x)
               (/ 1 (- (+ integer-part 1) fractional-part)))))
      (lambda ()
        (let ((b (f a)))
          (prog1 (append (if (zerop a) (list 0))
                         (list b (* -1 b)))
            (setf a b)))))))

(loop repeat 5
      do (print (funcall *rationals*)))
